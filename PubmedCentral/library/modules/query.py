#!/usr/bin/env python3

#script responsible for querying PubmedCentral database colleciton and storing data in local repo
#output: xml, medline, txt files built from query, __name__output_ids.txt

import os
import re
from Bio import Entrez, Medline
import datetime
import ast
import time

def __main__(wd):
	os.chdir(wd + '/library/inputs')
	inputDict = parseInput('input.txt')
	for i in range(0, int(inputDict['numQueries'])):
		inputDict['secondaryTerms'] = inputDict['secondaryTerms_' + str(i)]
		print('Running for query number ' + str(i) + ' with term ' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '\n')
		run(inputDict, wd)

def parseInput(text):
	with open(text) as file:
		inputDict = {}
		for line in file:
			line = line.strip()
			if not line.startswith('#'): inputDict[line.split('=')[0]] = line.split('=')[1]
	
	return(inputDict)

#Script Todo
def run(inputDict, wd):
	os.chdir(wd)
	Entrez.email = inputDict['email']
	
	now = datetime.datetime.now()
	outputName = ast.literal_eval(inputDict['primaryTerms'])[0].lower() + '_x_' + ast.literal_eval(inputDict['secondaryTerms'])[0].lower() + '_query_' + str(now.strftime('%Y_%m_%d')) + '_output'
	searchTerm = buildSearchTerm(ast.literal_eval(inputDict['primaryTerms']), ast.literal_eval(inputDict['secondaryTerms']), inputDict['sectionToSearch'], inputDict['startYear'], '5000')	
	ids = query('pmc', inputDict['maxDocuments'], searchTerm)
	
	os.chdir(os.getcwd() + '/output')
	
	out = open(outputName + '_ids.txt', 'w')
	for item in ids: out.write(item + '\n')
	out.close()

	os.chdir(os.getcwd() + '/files')
		
	print(str(len(ids)) + ' publications found matching search criteria')
	for i in range(0, len(ids)):
		passed = False
		while not passed:
			print('Parsing publication id ' + str(ids[i]) + ' ' + str(i + 1) + '/' + str(len(ids)))
			try:
				if (ids[i] + '.xml') not in os.listdir(os.getcwd()):
					xml = fetchXML('pmc',ids[i])
					buildXMLFile(ids[i], xml); print('Building xml file: ' + str(ids[i]) + '.xml')
				else: 
					with open(ids[i] + '.xml') as file: xml = file.read()
					file.close()

				if (ids[i] + '.medline') not in os.listdir(os.getcwd()):
					medline = fetchMedline('pmc', ids[i])
					buildMedlineFile(ids[i], medline); print('Building medline file: ' + str(ids[i]) + '.medline')
				else: 
					with open(ids[i] + '.medline') as file: medline = file.read()
					file.close()

				pars = parseXML(xml)

				if (ids[i] + '.txt') not in os.listdir(os.getcwd()): buildTxtFile(ids[i], medline, pars); print('Building txt file: ' + str(ids[i]) + '.txt')
				passed = True
				print()
			except Exception as e:
				print('Exception during parsing')
				time.sleep(2)
		
#Builds search term to query with and returns
def buildSearchTerm(primaryTerms, secondaryTerms, sectionToSearch, startYear, endYear):
	searchTerm = '((('
	for term in primaryTerms: searchTerm += term + sectionToSearch + ' OR '
	searchTerm = searchTerm[:-4] + ') AND ('
	for term in secondaryTerms: searchTerm += term + sectionToSearch + ' OR '
	searchTerm = searchTerm[:-4] + ')))'
	searchTerm += ' AND ("' + str(startYear) + '"[Publication Date] : "' + str(endYear) + '"[Publication Date])'

	return(searchTerm)

#Queries the Entrez database 'db' using query 'term' and returns list of pubmedIDs
def query(db, max, term):
	handle = Entrez.esearch(db = db, retmax = max, term = term)
	record = Entrez.read(handle)
	handle.close()

	return(record['IdList'])

#Fetches XML from database 'db' using id 'id' and returns the XML formatted string
def fetchXML(db, id):
	handle = Entrez.efetch(db = db, id = id, retmode = 'xml')
	xml = handle.read()
	handle.close()

	return(xml)

#Parses medline formatted string into plain text formatted string and returns
def fetchMedline(db, id):
	handle = Entrez.efetch(db = db, id = id, rettype = 'medline', retmode = 'text')
	medline = handle.read()
	handle.close()

	return(medline)

#Parses XML formatted string into plain text formatted string and returns
def parseXML(xml):
	pars = re.findall(r'(<p)(.+)', xml)
	for i in range(0, len(pars)):
		pars[i] = ''.join(pars[i])
		toRemove = re.findall(r'(<)([^>.]+)(>)', pars[i])
		for j in range(0, len(toRemove)): toRemove[j] = ''.join(toRemove[j])
		for item in toRemove: pars[i] = pars[i].replace(str(item), '')
	pars = '\n\n'.join(pars)

	return(pars)

#Builds the xml file
def buildXMLFile(id, xml):
	out = open((str(id) + '.xml'), 'w')
	out.write(xml)
	out.close()

#Builds the medline file
def buildMedlineFile(id, medline):
	out = open((str(id) + '.medline'), 'w')
	out.write(medline)
	out.close()

#Builds the txt file containing information from medline and the body paragraphs
def buildTxtFile(id, medline, pars):
	out = open((str(id) + '.txt'), 'w')
	out.write(medline + '\n-----TEXT-----\n\n' + pars)
	out.close()