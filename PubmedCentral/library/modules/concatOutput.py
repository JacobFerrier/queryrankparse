#!/usr/bin/env python3

import os
import datetime
import ast

def __main__(wd):
	os.chdir(wd + '/library/inputs')
	inputDict = parseInput('input.txt')
	for i in range(0, int(inputDict['numQueries'])):
		inputDict['secondaryTerms'] = inputDict['secondaryTerms_' + str(i)]
		print('Running for query number ' + str(i) + ' with term ' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '\n')
		run(inputDict, wd)

def parseInput(text):
	with open(text) as file:
		inputDict = {}
		for line in file:
			line = line.strip()
			if not line.startswith('#'): inputDict[line.split('=')[0]] = line.split('=')[1]
	
	return(inputDict)
		
def run(inputDict, wd):
	now = datetime.datetime.now()
	query = ast.literal_eval(inputDict['primaryTerms'])[0] + '_x_' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '_query_' + str(now.strftime('%Y_%m_%d')) + '_output.txt'
	possibleOrganisms = ast.literal_eval(inputDict['primaryTerms'])[0] + '_x_' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '_query_' + str(now.strftime('%Y_%m_%d')) + '_possible_organism_subject_output.txt'
	scores =  ast.literal_eval(inputDict['primaryTerms'])[0] + '_x_' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '_query_' + str(now.strftime('%Y_%m_%d')) + '_corpus_score_output.txt'
	
	os.chdir(wd + '/output')
	outputDir = os.getcwd()
	
	queryDictionary = parseOutput(query, 11, 10, 1)
	possibleOrganismsDictionary = parseOutput(possibleOrganisms, 1, 0, 0)
	scoresDictionary = parseOutput(scores, 1, 0, 0)
	infoDictionary = getInfo(query)
	
	mergedDictionary = {}
	for key in queryDictionary.keys():
		mergedDictionary[key] = queryDictionary[key]
		
		if (key in possibleOrganismsDictionary.keys()):
			mergedDictionary[key].extend(possibleOrganismsDictionary[key][1:])
		else:
			mergedDictionary[key].extend(['NaN'] * (len(possibleOrganismsDictionary['Reference']) - 1))
		
		if (key in scoresDictionary.keys()):
			mergedDictionary[key].extend(scoresDictionary[key][1:])
		else:
			mergedDictionary[key].extend(['NaN'] * (len(scoresDictionary['Reference']) - 1))
	
	out = open(query.replace('output.txt','report.txt'), 'w')
	
	for key in infoDictionary.keys():
		if key == 'Reference':
			pass
		else:
			out.write(key + '\t' + str(infoDictionary[key]) + '\n')
	out.write('\n\n')
	
	for key in mergedDictionary.keys():
		for value in mergedDictionary[key]:
			out.write(str(value) + '\t')
		out.write('\n')
	
def getInfo(output):
	with open(output) as file:
		lineList = []
		for line in file: lineList.append(line)
	file.close()
	
	infoDictionary = {}
	infoDictionary['Reference'] = ['Metric','Value']
	for i in range(0,8): infoDictionary[lineList[i].strip().split('\t')[0]] = lineList[i].strip().split('\t')[1]
	
	if 'N/A' in infoDictionary.keys(): del infoDictionary['N/A']
	
	return(infoDictionary)
	
def parseOutput(output, startPos, refPos, indexPos):
	with open(output) as file:
		lineList = []
		for line in file: lineList.append(line)
	file.close()
	
	dictionary = {}
	dictionary['Reference'] = lineList[refPos].strip().split('\t')
	for i in range(startPos, len(lineList)): dictionary[lineList[i].strip().split('\t')[indexPos]] = lineList[i].strip().split('\t')
	
	if 'N/A' in dictionary.keys(): del dictionary['N/A']
	
	return(dictionary)