#!/usr/bin/env python3

import os
import datetime
import ast
import re
from collections import Counter

def __main__(wd):
	os.chdir(wd + '/library/inputs')
	inputDict = parseInput('input.txt')
	for i in range(0, int(inputDict['numQueries'])):
		inputDict['secondaryTerms'] = inputDict['secondaryTerms_' + str(i)]
		print('Running for query number ' + str(i) + ' with term ' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '\n')
		run(inputDict, wd)
		
def parseInput(text):
	with open(text) as file:
		inputDict = {}
		for line in file:
			line = line.strip()
			if not line.startswith('#'): inputDict[line.split('=')[0]] = line.split('=')[1]
	
	return(inputDict)

def run(inputDict, wd):
	now = datetime.datetime.now()
	inputIDName = ast.literal_eval(inputDict['primaryTerms'])[0].lower() + '_x_' + ast.literal_eval(inputDict['secondaryTerms'])[0].lower() + '_query_' + str(now.strftime('%Y_%m_%d')) + '_output_ids.txt'
	
	os.chdir(wd + '/output')
	ids = parseInputID(inputIDName)
	
	out = open(inputIDName.replace('output_ids.txt','possible_organism_subject_output.txt'), 'w')
	out.write('PMC_ID\tOrganism_Subjects\n')
	
	for i in range(0, len(ids)):
		os.chdir(wd + '/output/files')
		print('Interpolating names from publication id ' + str(ids[i]) + ' ' + str(i+1) + '/' + str(len(ids)))
		with open(ids[i] + '.xml') as file: xml = file.read(); file.close()
		
		startIter = [m.start() for m in re.finditer('<italic>', xml)]
		endIter = [m.start() for m in re.finditer('</italic>', xml)]
		
		candidates = []
		for j in range(0, len(startIter)): candidates.append(xml[startIter[j]+8:endIter[j]].strip())
		if (len(Counter(candidates).keys()) != 0):
			maxCandidates = getMaxKeys(Counter(candidates))
		else:
			maxCandidates = ['NAN']
		
		os.chdir(wd + '/output')
		out.write('PMC' + str(ids[i]) + '\t' + str(maxCandidates) + '\n')
		print()
	
def parseInputID(text):
	with open(text) as file:
		lineList = []
		for line in file: lineList.append(line.strip())
			
	return(lineList)

def getMaxKeys(dict):
	keys, items = [], []
	restrictedPhrases = ['Plant Physiol.', 'New Phytol.', 'Bmc Genomics', 'Acta Horticulturae']
	for key in dict.keys():
		if (len(key) > 10 and len(key.split(' ')) == 2 and key.count('.') < 2 and key not in restrictedPhrases and ',' not in key):
			keys.append(key)
			items.append(dict[key])
	
	if (len(keys) != 0):	
		maxValue = max(items)
		maxKeys = []
		for key in keys:
			if (dict[key] == maxValue): maxKeys.append(key)
	else:
		maxKeys = ['NAN']
	
	return(maxKeys)