#!/usr/bin/env python3

#script responsible for scoring corpus via tf-idf methodology
#output: __name__output_corpus_score.txt

import os
import math
import datetime
import ast

def __main__(wd):
	os.chdir(wd + '/library/inputs')
	inputDict = parseInput('input.txt')
	for i in range(0, int(inputDict['numQueries'])):
		inputDict['secondaryTerms'] = inputDict['secondaryTerms_' + str(i)]
		print('Running for query number ' + str(i) + ' with term ' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '\n')
		run(inputDict, wd)

def parseInput(text):
	with open(text) as file:
		inputDict = {}
		for line in file:
			line = line.strip()
			if not line.startswith('#'): inputDict[line.split('=')[0]] = line.split('=')[1]
	
	return(inputDict)

def run(inputDict, wd):
	now = datetime.datetime.now()
	query = ast.literal_eval(inputDict['primaryTerms'])[0] + '_x_' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '_query_' + str(now.strftime('%Y_%m_%d')) + '_output.txt'
	
	os.chdir(wd + '/output')
	outputDir = os.getcwd()
	queryDictionary = parseQuery(query)
	if 'N/A' in queryDictionary.keys(): del queryDictionary['N/A']
	
	titleDictionary = {}
	totalRankingTermHits = {}
	for key in queryDictionary.keys():
		if key not in ['Reference','Primary_Terms','Secondary_Terms','Tertiary_Terms','Publications_Found']:
			titleDictionary[key] = queryDictionary[key][3]
			totalRankingTermHits[key] = queryDictionary[key][-1]
	
	os.chdir(os.getcwd() + '/files')
	corpusDictTermList = buildCorpusDictTermList(queryDictionary)
	corpusDictTFScore = buildCorpusDictTFScore(corpusDictTermList)
	corpusDictDocScore = buildCorpusDictTF_IDFScore(corpusDictTermList, corpusDictTFScore)
	subjectCountDict = buildSubjectCountDict(list(corpusDictTermList.keys()))
	
	os.chdir(outputDir)
	out = open(query.replace('output.txt','corpus_score_output.txt'), 'w')
	out.write('PMC_ID\tTF-IDF_Score\tTotal_Hits_Ranking_Terms\tPlant_Count\tBacteria_Count\tY_Hat\tPrediction\tModel_Certainty\n')
	for key in corpusDictDocScore.keys(): out.write(key + '\t' + str(corpusDictDocScore[key]) + '\t' + str(totalRankingTermHits[key]) + '\t' +
													str(subjectCountDict[key][0]) + '\t' + str(subjectCountDict[key][1]) + '\t' + str(subjectCountDict[key][2]) + '\t' +
													str(subjectCountDict[key][3]) + '\t' + str(subjectCountDict[key][4]) + '\n')
	out.close()

def parseQuery(query):
	with open(query) as file:
		lineList = []
		for line in file: lineList.append(line)
	file.close()
	
	queryDictionary = {}
	for i in range(11, len(lineList)): queryDictionary[lineList[i].strip().split('\t')[1]] = lineList[i].strip().split('\t')
	
	queryDictionary['Primary_Terms'] = lineList[3].split('\t')[1].strip()
	queryDictionary['Secondary_Terms'] = lineList[4].split('\t')[1].strip()
	queryDictionary['Tertiary_Terms'] = lineList[5].split('\t')[1].strip()
	queryDictionary['Publications_Found'] = lineList[7].split('\t')[1].strip()
	queryDictionary['Reference'] = lineList[10].strip().split('\t')
	
	return(queryDictionary)

def parseTextFile(name):
	with open(name) as file:
		string = file.read()
	file.close()

	text = string.split('-----TEXT-----')[1]
	
	return(text)

def buildCorpusDictTermList(queryDictionary):
	corpusDictTermList = {}	
	for key in queryDictionary.keys():
		if key not in ['Reference','Primary_Terms','Secondary_Terms','Tertiary_Terms','Publications_Found']:
			with open(key[3:] + '.xml') as file:
				string = file.read()
				if '<!--The publisher of this article does not allow downloading of the full text in XML form.-->' not in string: corpusDictTermList[key] = queryDictionary[key]
				file.close()
	
	for key in corpusDictTermList.keys():
		text = parseTextFile(key[3:] + '.txt')
		split = [item.lower() for item in text.split()]
		corpusDictTermList[key].append(len(split))
		corpusDictTermList[key] = corpusDictTermList[key][12:-2] + corpusDictTermList[key][-1:]
	
	referenceTermList = queryDictionary['Reference']
	referenceTermList.append('Total_Words')
	referenceTermList = referenceTermList[12:-2] + referenceTermList[-1:]
	for i in range(0, len(referenceTermList)): referenceTermList[i] = referenceTermList[i].replace('_Hits_Full','')
	corpusDictTermList['Reference'] = referenceTermList
	
	return(corpusDictTermList)

def buildCorpusDictTFScore(corpusDictTermList):
	corpusDictTFScore = {}
	for key in corpusDictTermList.keys():
		if key != 'Reference':
			squareSums = 0
			for i in range(0, len(corpusDictTermList[key])-1): squareSums += int(corpusDictTermList[key][i]) ** 2
			euclideanNorm = math.sqrt(squareSums)
			
			corpusDictTFScore[key] = []
			for i in range(0, len(corpusDictTermList[key])-1):
				if euclideanNorm == 0: corpusDictTFScore[key].append(0.0)
				else: corpusDictTFScore[key].append(int(corpusDictTermList[key][i])/euclideanNorm)
	
	corpusDictTFScore['Reference'] = corpusDictTermList['Reference'][:-1]
	
	return(corpusDictTFScore)

def buildCorpusDictTF_IDFScore(corpusDictTermList, corpusDictTFScore):
	corpusN = len(corpusDictTermList.keys()) - 1
	termList = corpusDictTermList['Reference'][:-1]
	
	termFrequencyDict = {}
	for term in termList:
		scanIndex = termList.index(term)
		termAppearances = 0
		for key in corpusDictTermList.keys():
			if key != 'Reference':
				if (int(corpusDictTermList[key][scanIndex]) > 0): termAppearances += 1
		termFrequencyDict[term] = termAppearances
	
	IDFScoreDict = {}
	for term in termFrequencyDict.keys():
		if (termFrequencyDict[term] == 0): IDFScoreDict[term] = 0.0
		else: IDFScoreDict[term] = math.log10(corpusN/termFrequencyDict[term])
		
	IDFScoreList = []
	for term in IDFScoreDict.keys(): IDFScoreList.append(IDFScoreDict[term])
	
	corpusDictTF_IDFScore = {}
	for key in corpusDictTFScore.keys():
		if key != 'Reference':
			corpusDictTF_IDFScore[key] = []
			for i in range(0, len(corpusDictTFScore[key])): corpusDictTF_IDFScore[key].append(corpusDictTFScore[key][i] * IDFScoreList[i])
	
	corpusDictDocScore = {}
	for key in corpusDictTF_IDFScore.keys(): corpusDictDocScore[key] = sum(corpusDictTF_IDFScore[key])
		
	return(corpusDictDocScore)

def calcYHat(x,w,b):
	return(dotProduct(x,w) + b)

def dotProduct(x,w):
	sum = 0
	for i in range(0, len(x)): sum += x[i] * w[i]
	return(sum)

def sign(x):
	if x > 0: return(1)
	if x < 0: return(-1)
	else: return(0)
	
def sigmoid(x):
	return(1 / (1 + math.pow(math.e, -(x))))

def buildSubjectCountDict(idList):
	if 'Reference' in idList: idList.remove('Reference')
	
	subjectCountDict = {}
	weights = [1,-1]
#	subjectCountDict['Reference'] = ['plantCount','bacteriaCount','yHat','prediction','certainty']
	
	for id in idList:
		text = parseTextFile(id[3:] + '.txt')
		plantCount = text.count('plant')
		bacteriaCount = text.count('bacteria')
		counts = [plantCount, bacteriaCount]
		yHat = calcYHat(counts, weights, 0)
		subjectCountDict[id] = [plantCount, bacteriaCount, yHat, sign(yHat), float('%0.10f' % sigmoid(yHat))]
	
	return(subjectCountDict)
		