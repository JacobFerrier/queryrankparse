#!/usr/bin/env python3

#script responsible for responding to the PubmedCentral query by providing output via tab delimited file containing information on word content
#output: __name__output.txt

import datetime
import os
import re
import ast

def __main__(wd):
	os.chdir(wd + '/library/inputs')
	inputDict = parseInput('input.txt')
	for i in range(0, int(inputDict['numQueries'])):
		inputDict['secondaryTerms'] = inputDict['secondaryTerms_' + str(i)]
		print('Running for query number ' + str(i) + ' with term ' + ast.literal_eval(inputDict['secondaryTerms'])[0] + '\n')
		run(inputDict, wd)

def parseInput(text):
	with open(text) as file:
		inputDict = {}
		for line in file:
			line = line.strip()
			if not line.startswith('#'): inputDict[line.split('=')[0]] = line.split('=')[1]
	
	return(inputDict)

def parseInputID(text):
	with open(text) as file:
		lineList = []
		for line in file: lineList.append(line.strip())
			
	return(lineList)
	
def run(inputDict, wd):
	now = datetime.datetime.now()
	inputIDName = ast.literal_eval(inputDict['primaryTerms'])[0].lower() + '_x_' + ast.literal_eval(inputDict['secondaryTerms'])[0].lower() + '_query_' + str(now.strftime('%Y_%m_%d')) + '_output_ids.txt'
	primaryTerms = ast.literal_eval(inputDict['primaryTerms'])
	secondaryTerms = ast.literal_eval(inputDict['secondaryTerms'])
	tertiaryTerms = ast.literal_eval(inputDict['tertiaryTerms'])
	
	os.chdir(wd + '/output')
	ids = parseInputID(inputIDName)
	
	now = datetime.datetime.now()
	outputName = ast.literal_eval(inputDict['primaryTerms'])[0].lower() + '_x_' + ast.literal_eval(inputDict['secondaryTerms'])[0].lower() + '_query_' + str(now.strftime('%Y_%m_%d')) + '_output'
	searchTerm = buildSearchTerm(ast.literal_eval(inputDict['primaryTerms']), ast.literal_eval(inputDict['secondaryTerms']), inputDict['sectionToSearch'], inputDict['startYear'], '5000')
	
	out = open((str(outputName) + '.txt'), 'w')
	out.write('Database_Queried:\t' + 'Pubmed_Central ' + '\nQuery_Name:\t' + outputName[:-7] + '\nQuery_Term:\t' + searchTerm + '\nPrimary_Terms:\t'
			  + str(ast.literal_eval(inputDict['primaryTerms'])) + '\nSecondary_Terms:\t' + str(ast.literal_eval(inputDict['secondaryTerms'])) + '\nTertiary_Terms:\t' + str(tertiaryTerms)
			  + '\nMax_Publications_Queried:\t' + str(inputDict['maxDocuments']) + '\nPublications_Found:\t' + str(len(ids)) + '\n\n\n')
	out.write('Pubmed_ID\t' + 'Pubmed_Central_ID\t' + 'Year\t'  + 'Title\t' + 'Abstract_Phrase\t' + 'Link_To_PDF\t' +
			  'Primary_Hits_Title\t' + 'Secondary_Hits_Title\t' + 'Total_Hits_Primary_Secondary_Title\t' +
			  'Primary_Hits_Full\t' + 'Secondary_Hits_Full\t' + 'Total_Hits_Primary_Secondary_Full\t')
	for key in tertiaryTerms.keys(): out.write(key + '_Hits_Full\t')
	out.write('Total_Hits_Tertiary_Full\n')
	out.close()
	
	orgDir = os.getcwd()
	
	for i in range(0, len(ids)):
		print('Writing data from publication id ' + str(ids[i]) + ' ' + str(i + 1) + '/' + str(len(ids)) + '\n')
		os.chdir(orgDir + '/files')
		with open(ids[i] + '.xml') as file: xml = file.read(); file.close()
		with open(ids[i] + '.medline') as file: medline = file.read(); file.close()
		pars = parseXML(xml)

		os.chdir(orgDir)
		buildTabFile(medline, pars, outputName, primaryTerms, secondaryTerms, tertiaryTerms)

#Builds search term to query with and returns
def buildSearchTerm(primaryTerms, secondaryTerms, sectionToSearch, startYear, endYear):
	searchTerm = '((('
	for term in primaryTerms: searchTerm += term + sectionToSearch + ' OR '
	searchTerm = searchTerm[:-4] + ') AND ('
	for term in secondaryTerms: searchTerm += term + sectionToSearch + ' OR '
	searchTerm = searchTerm[:-4] + ')))'
	searchTerm += ' AND ("' + str(startYear) + '"[Publication Date] : "' + str(endYear) + '"[Publication Date])'

	return(searchTerm)

#Parses XML formatted string into plain text formatted string and returns
def parseXML(xml):
	pars = re.findall(r'(<p)(.+)', xml)
	for i in range(0, len(pars)):
		pars[i] = ''.join(pars[i])
		toRemove = re.findall(r'(<)([^>.]+)(>)', pars[i])
		for j in range(0, len(toRemove)): toRemove[j] = ''.join(toRemove[j])
		for item in toRemove: pars[i] = pars[i].replace(str(item), '')
	pars = '\n\n'.join(pars)

	return(pars)

#Builds the tab delimited table file containing information on hits
def buildTabFile(medline, pars, name, primaryTerms, secondaryTerms, tertiaryTerms):
	out = open((str(name) + '.txt'), 'a')
	medlineSplit = medline.split('\n')
	PMC_ID, PM_ID, Year, Title, Abstract_Phrase = 'NaN', 'NaN', 'NaN', 'NaN', 'NaN'
	for i in range(0, len(medlineSplit)):
		if medlineSplit[i].startswith('PMC'): PMC_ID = medlineSplit[i][6:]
		if medlineSplit[i].startswith('PMID'): PM_ID = medlineSplit[i][6:]
		if medlineSplit[i].startswith('DP'): Year = medlineSplit[i][6:]
		if medlineSplit[i].startswith('TI'):
			Title = medlineSplit[i][6:]
			if medlineSplit[i+1].endswith('.'): Title += medlineSplit[i+1].strip()

	'''
	Primary_Hits_Full, Secondary_Hits_Full = pars.lower().count(terms[0].lower()), 0
	for i in range(1, len(terms)): Secondary_Hits_Full += pars.lower().count(terms[i].lower())
	Total_Hits_Full = Primary_Hits_Full + Secondary_Hits_Full

	Primary_Hits_Title, Secondary_Hits_Title = Title.lower().count(terms[0].lower()), 0
	for i in range(1, len(terms)): Secondary_Hits_Title += Title.lower().count(terms[i].lower())
	Total_Hits_Title = Primary_Hits_Title + Secondary_Hits_Title
	'''
	
	Primary_Hits_Full, Secondary_Hits_Full = 0,0
	for term in primaryTerms: Primary_Hits_Full += pars.lower().count(term.lower())
	for term in secondaryTerms: Secondary_Hits_Full += pars.lower().count(term.lower())
	Total_Hits_Full = Primary_Hits_Full + Secondary_Hits_Full
		
	Primary_Hits_Title, Secondary_Hits_Title = 0,0
	for term in primaryTerms: Primary_Hits_Title += Title.lower().count(term.lower())
	for term in secondaryTerms: Secondary_Hits_Title += Title.lower().count(term.lower())
	Total_Hits_Title = Primary_Hits_Title + Secondary_Hits_Title
	
	for sentence in medline.split('.'):
		handle = re.search(r'(in this study)', sentence, flags=re.IGNORECASE)
		if (handle):
			sentence = sentence.strip().replace('\t','')
			sentence = sentence.replace('\n','')
			sentence = sentence.replace('     ','')
			Abstract_Phrase = sentence.strip()

	Link = 'https://www.ncbi.nlm.nih.gov/pmc/articles/' + PMC_ID +'/'

	tHitDict, hits, totalTHits = {}, 0, 0
	for key in tertiaryTerms.keys():
		hits = 0
		for phrase in tertiaryTerms[key]:
			hits += pars.lower().count(phrase.lower())
		tHitDict[key] = hits
		totalTHits += hits

	out.write(PM_ID + '\t' + PMC_ID + '\t' + Year + '\t' + Title + '\t' + Abstract_Phrase + '\t' + str(Link) + '\t' +
			  str(Primary_Hits_Title) + '\t' + str(Secondary_Hits_Title) + '\t' + str(Total_Hits_Title) + '\t' +
			  str(Primary_Hits_Full) + '\t' + str(Secondary_Hits_Full) + '\t' + str(Total_Hits_Full) + '\t')
	for key in tHitDict: out.write(str(tHitDict[key]) + '\t')
	out.write(str(totalTHits) + '\n')
	out.close()