#!/usr/bin/env python3

import os

mainDir = os.getcwd()

try:
	print('\n-----Wrapping input-----\n')
	from library.modules import manageInput
	manageInput.__main__(mainDir)
	print('Done')
except Exception as e:
	print('Exception found during query: ' + str(e))
	exit()

try:
	print('\n-----Checking dependencies-----\n')
	from library.modules import dependencies
	dependencies.__main__(mainDir)
	print('Done')
except FileNotFoundError as e:
	print('File input.txt not found, example file has been produced however fields still need to be filled in')
	exit()
except Exception as e:
	print('Exception found during dependency checking: ' + str(e))
	exit()

try:
	print('\n-----Querying Entrez databases-----\n')
	from library.modules import query
	query.__main__(mainDir)
	print('Done')
except Exception as e:
	print('Exception found during query: ' + str(e))
	exit()

try:
	print('\n-----Responding to query-----\n')
	from library.modules import queryRespond
	queryRespond.__main__(mainDir)
	print('Done')
except Exception as e:
	print('Exception found during query response: ' + str(e))
	exit()

try:
	print('\n-----Ranking corpus-----\n')
	from library.modules import ranking
	ranking.__main__(mainDir)
	print('Done')
except Exception as e:
	print('Exception found during ranking: ' + str(e))
	exit()
	
try:
	print('\n-----Exctracting names-----\n')
	from library.modules import naming
	naming.__main__(mainDir)
	print('Done\n')
except Exception as e:
	print('Exception found during naming: ' + str(e))
	exit()
	
try:
	print('\n-----Concatinating output-----\n')
	from library.modules import concatOutput
	concatOutput.__main__(mainDir)
	print('Done\n')
except Exception as e:
	print('Exceptin found during concating output: ' + str(e))
	exit()